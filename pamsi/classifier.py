from sklearn.neural_network import MLPClassifier


def train(X, y):
    """
    Usage example:
    >>> from pamsi.classifier import train
    >>> classifier = train(df[["Src Port", "Flow Duration"]], df["Label_int"])
    >>> # Just to make an example here, I'm scoring on the training data, separate
    >>> # training and scoring datasets should be used in a real-life scenario
    >>> classifier.score(df[["Src Port", "Flow Duration"]], df["Label_int"])
    """
    hidden_layer_width = 50
    momentum_value = 0.0
    momentum = False

    mlp = MLPClassifier(
        hidden_layer_sizes=(hidden_layer_width,),
        max_iter=1000,
        alpha=0.0001,
        momentum=momentum_value,
        nesterovs_momentum=momentum,
        solver="sgd",
        verbose=False,
        tol=0.0001,
        random_state=1234,
    )
    mlp.fit(X, y)
    return mlp
