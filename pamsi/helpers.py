import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split


def printAllUniqueValuesFromAllColumns(df):
    for column in df.columns:
        print(column)
        print(df[str(column)].unique())
        print("________________________________________________")


def convertToNumericValues(df):
    # Convertion to numeric values
    for column in df.columns:
        if not any(
            substring in column for substring in ("ID", "IP", "Timestamp", "Label")
        ):
            df[column] = pd.to_numeric(df[column])


def showHistograms(df):
    # Histograms
    for column in df.columns:
        if not any(substring in column for substring in ("ID", "IP", "Timestamp")):
            if any(substring in column for substring in ("Packet", "Flow")):
                df[column] = df[column][df[column] < 1000]
            print(column)
            plt.hist(df[column], bins=100)
            plt.show()


def printAllColumns(df):
    # all columns
    for col in df.columns:
        print(col)


def printColumnsTypes(df):
    for type1 in df.dtypes:
        print(type1)


def calculateDataContinuousness(df):
    sorted_timestamps = df["Timestamp"].sort_values()
    df["Timestamp"] = pd.to_datetime(df["Timestamp"])
    # Make sure that the converted value is correct
    df["Timestamp"].dtype
    # Make sure that the converted value is correct
    df["Timestamp"].dtype
    first_timestamp = sorted_timestamps.iloc[0].to_pydatetime()
    last_timestamp = sorted_timestamps.iloc[-1].to_pydatetime()

    # Decided to go week by week as per-day resolution resulted in unreadable plots
    dataset_span_weeks = (last_timestamp - first_timestamp).days // 7
    dataset_span_weeks
    plt.hist(df["Timestamp"], bins=dataset_span_weeks)
    plt.legend(["Liczba pomiarów w danym tygodniu"])
    plt.title("Ciągłość danych")
    plt.show()


def calculateAndShowAbsoluteCorrelationWithAllColumns(df):
    corr = df.corr().abs()
    corr.style.background_gradient(cmap="coolwarm")


def showSmallestCorrelationBetweenAllColumns(df):
    df.corr().abs().unstack().sort_values().drop_duplicates()[0:10]


def splitDataSetToTrainingSetAndTestingSet(df):
    training_data, testing_data = train_test_split(df, test_size=0.2, random_state=25)

    return training_data, testing_data



