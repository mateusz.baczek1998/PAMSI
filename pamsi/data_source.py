import ipaddress

import numpy as np
import pandas as pd
import datetime


def load_raw_data() -> pd.DataFrame:
    df = pd.read_csv("Darknet.CSV")

    # No idea why pandas does this, but row 141530 contains the column names
    # and subsequent rows are the repeated data from the previous lines of dataset
    # (row 141531 is row 0 and so forth). I'm trimming the DataFrame after loading to prevent conversion issues
    df = df[:141530]

    return df


def timestamp_to_float(df: pd.DataFrame):
    float_timestamps = [t.timestamp() for t in df["Timestamp"]]
    df["Timestamp_float"] = pd.Series(float_timestamps)


def label_to_int(df: pd.DataFrame):
    label_to_number = {
        "Non-Tor": 0,
        "Tor": 1
    }
    label_int = [label_to_number[label] for label in df["Label"] ]
    df = df.assign(Label_int=label_int)
    return df
    

def delete_vpn_related_records(df: pd.DataFrame):
    initial_size = len(df["Label"].index)

    df = df[ ( df["Label"] != "VPN" ) ]
    df = df[ ( df["Label"] != "NonVPN" ) ]

    new_size = len(df["Label"].index)
    print(f"Removed {initial_size - new_size} non-Tor related records")

    return df


def split_dataframe_to_periods(df: pd.DataFrame):
    df_pre_jul2015 = df.loc[(df["Timestamp"] < datetime.datetime(2015, 7, 1))]
    df_jul2015_to_jan2016 = df.loc[
        (df["Timestamp"] >= datetime.datetime(2015, 7, 1)) & (df["Timestamp"] <= datetime.datetime(2016, 1, 1))]
    df_post_jan2016 = df.loc[(df["Timestamp"] > datetime.datetime(2016, 1, 1))]

    return df_pre_jul2015, df_jul2015_to_jan2016, df_post_jan2016


def fix_datatypes(df: pd.DataFrame):
    # Fix timestamp
    df["Timestamp"] = pd.to_datetime(df["Timestamp"])
    timestamp_to_float(df)

    # Use python's built-in ipaddress types for IPs
    # adresses = []
    # for ip in df["Src IP"]:
    #     adresses.append(ipaddress.ip_address(ip))
    # df["Src IP"] = adresses

    # adresses = []
    # for ip in df["Dst IP"]:
    #     adresses.append(ipaddress.ip_address(ip))
    # df["Dst IP"] = adresses

    for column in df.columns:
        if not any(
            substring in column for substring in ("ID", "IP", "Timestamp", "Label", "Dst IP", "Src IP")
        ):
            df[column] = pd.to_numeric(df[column])


def delete_useless_columns(df: pd.DataFrame):
    """Drop columns with no unique values"""
    df.drop(
        columns=["Active Mean", "Active Std", "Active Max", "Active Min", "Flow ID"], inplace=True
    )


def print_uniques(df: pd.DataFrame):
    for column in df.columns:
        print(column)
        print(df[str(column)].unique())
        print("________________________________________________")


def drop_NA_rows(df: pd.DataFrame):
    count_pre = len(df.index)
    df.replace([np.inf, -np.inf], np.nan, inplace=True)
    df.dropna(inplace=True)
    count_post = len(df.index)
    print(f"{count_pre - count_post} records have been dropped because of missing values.")


def load_dataset():
    df = load_raw_data()
    delete_useless_columns(df)
    df = delete_vpn_related_records(df)
    fix_datatypes(df)
    print(df.shape)
    drop_NA_rows(df)
    df = label_to_int(df)
    print(df.shape)

    return df

def splitAtributesFromResults(df):
    
    feature = df.iloc[:, -4]
    atributes = df.iloc[:,0:-3]
    
    return atributes, feature


if __name__ == "__main__":
    pd.options.display.max_columns = 90
    pd.options.display.max_rows = 90

    df = load_dataset()

    print(df.dtypes)
    # df = load_data()
    # fix_datatypes(df)
    # delete_useless_columns(df)
    # print_uniques(df)
    # print(df.dtypes)
